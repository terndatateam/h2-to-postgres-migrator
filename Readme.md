## H2 to Postgres data migration tool

This tool has been developed as part of the "Apache Airflow Ecoplatform pipeline" and, even if it could work fine for other purposes, it does many specific task related to airlfow and the ecoplatform pipeline.

The migration between H2 and Postgres has been resolved easily using Apache Spark to load a whole H2 database file into memory (as spark dataframe) and save it in a postgres database. 

#### Dataset configuration

Each possible dataset to migrate needs a configuration JSON file (/dataset/{{dataset}}.json):

```buildoutcfg
{
  "spark": {
    "master": "local[*]",
    "app_name": "CORVEG_H2_MIGRATOR_SPARK"
  },
  "h2": {
    "url": "jdbc:h2:/temp/corveg"
  },
  "postgres": {
    "dataset_schema": "corveg",
    "executions_schema": "executions"
  },
  "tables": [
    "site"
  ],
  "table_schemas": {
    "site": "site_id INTEGER,project_id SHORT,location_id INTEGER,description STRING,site_number STRING,site_date TIMESTAMP,entry_date TIMESTAMP,litter_percent DECIMAL(5, 2),rock_percent DECIMAL(5, 2),bare_percent DECIMAL(5, 2),crypto_percent DECIMAL(5, 2),samplelevel_id SHORT,sampletype_id SHORT,samplefloristics_id SHORT,sample_area INTEGER,has_photo BOOLEAN,has_basal_bystrata BOOLEAN,has_cover_bystrata BOOLEAN,has_stem_bystrata BOOLEAN,has_basal_bytaxa BOOLEAN,has_cover_bytaxa BOOLEAN,has_stem_bytaxa BOOLEAN,checked BOOLEAN,update_user SHORT,last_update TIMESTAMP"
  },
  "diff_tables": [
    "site"
  ]
}
```

- **h2.url**: JDBC url format pointing to the H2 file (NOTICE that the file extension ".h2.db" must be skip in the url)

- **postgres.dataset_schema**: dataset name, where the migrated db from H2 will be stored. To ensure the pipeline will work, simply just the dataset name.
- **postgres.executions_schema**: schema where statistics about differences between versions will be stored.
- **tables**: table name to migrate
- **table_schemas**: if necessary (e.g. corveg), write the spark dataset schema as the example. This is because in corveg H2, DECIMAL fields have so many decimal precision (unnecessary) and spark fail, and other fields apart from DECIMAL are VARCHARS.
- **diff_tables**: all the tables the difference between version will be calculated. 

##### Postgres connection

Postgres connection information is taken from the following Environment variables:

```buildoutcfg
pg_host = os.getenv('POSTGRES_HOST')
pg_port = os.getenv('POSTGRES_PORT')
pg_database = os.getenv('POSTGRES_DB')
pg_username = os.getenv('POSTGRES_USERNAME')
pg_password = os.getenv('POSTGRES_PASSWORD')
```

#### Usage

```buildoutcfg
python migrator.py --dataset AUSPLOTS --dagrun_id ecoplatform_pipeline_ausplots_manual__2020-03-24T01:24:45.412383+00:00
```

#### Migration process

1. Rename the postgres schema {{dataset}} into {{dataset}}_{{ingestion_time}}, where ingestion time is the date when it was created in postgres.
2. Load tables (one by one) into memory as Spark Dataframe, using the defined schema if necessary.
3. Save the dataframes in the postgres database "ecoplatform", schema "{{dataset}}"
3. Calculate the differences between {{dataset}} schema and its previous version {{dataset}}_{{ingestion_time}}, and store it in the executions schema.
