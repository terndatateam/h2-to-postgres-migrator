from pyspark.sql import SparkSession, DataFrame
import psycopg2 as pg
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import uuid


def get_postgres_url(host, port, dbname, username=None, password=None):
    """
    Build a postgres DB URI as a String
    :param host: DB host
    :param port: DB port
    :param dbname: DB database
    :param username: DB username
    :param password: DB password
    :return: URI as a String
    """
    tcpKeepAlive = True
    socketTimeout = 120 # 120 seconds
    connectTimeout = 120
    loginTimeout = 120
    
    if username is not None and password is not None:
        return "jdbc:postgresql://{}:{}/{}?user={}&password={}&tcpKeepAlive={}&socketTimeout={}&connectTimeout={}&loginTimeout={}".format(host, port, dbname, username, password, tcpKeepAlive, socketTimeout, connectTimeout, loginTimeout)
    else:
        return "jdbc:postgresql://{}:{}/{}".format(host, port, dbname)


def load_table_pg(spark: SparkSession, url: str, dbname: str, schema: str, table_name: str, username,
                  password) -> DataFrame:
    """
    Get a Spark DataFrame object from a remote database table.

    :param spark: SparkSession
    :param url: JDBC url
    :param dbname: Database name
    :param schema: Postgres schema
    :param table_name: Table name
    :param username: Database username
    :param password: Database password
    :return: Spark DataFrame
    """

    df = spark.read \
        .format('jdbc') \
        .options(url=url, \
                 database=dbname, \
                 dbtable="{}.{}".format(schema, table_name), \
                 user=username, \
                 password=password) \
        .load()

    return df


def load_table_h2(spark: SparkSession, url: str, table_name: str, df_schema: str) -> DataFrame:
    """
    Get a Spark DataFrame object from a remote database table (H2).

    :param spark: SparkSession
    :param url: JDBC url
    :param table_name: Table name
    :param df_schema: Spark SQL customSchema
    :return: Spark DataFrame
    """

    df = spark.read \
        .format('jdbc') \
        .options(url=url, \
                 dbtable=table_name, \
                 customSchema=df_schema) \
        .load()

    return df


def save_table_pg(df: DataFrame, url: str, schema: str, table_name: str, username: str, password: str):
    """
    Save a Spark DataFrame object to a remote database table.

    :param df: Spark Dataframe to save into postgres table
    :param url: JDBC url
    :param schema: Postgres schema (i.e. dataset name)
    :param table_name: Table name
    :param username: DB username
    :param password: DB password
    """
    df.write.jdbc(url, "{}.{}".format(schema, table_name), properties={"user": username, "password": password})


def rename_schema(host, port, database, schema, username, password):
    """
    Rename the current schema (i.e. dataset) name with the ingestion time to keep it as a backup

    :param host: DB host
    :param port: DB port
    :param database: DB name
    :param schema: DB postgres schema (i.e. dataset name)
    :param username: DB username
    :param password: DB password
    :return: renamed schema as a String
    """
    con = pg.connect(dbname=database, host=host, port=port, user=username, password=password, sslmode=None)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    try:
        cur.execute(sql.SQL("SELECT load_date FROM {}.r_ecoplatform_metadata").format(sql.Identifier(schema)))
        load_date = cur.fetchone()[0]
        # cur.execute(sql.SQL(
        #     "SELECT pg_terminate_backend( pid ) FROM pg_stat_activity WHERE pid <> pg_backend_pid( ) AND datname = '{}';").format(
        #     sql.Identifier(database)))
        backup_schema_name = "{}_{}_{}".format(schema, load_date, uuid.uuid4()).replace("-", "")
        cur.execute(
            sql.SQL("ALTER SCHEMA {} RENAME TO {};").format(sql.Identifier(schema),
                                                            sql.Identifier(backup_schema_name)))
        print("Previous schema renamed with success to '{}'".format(backup_schema_name))
    except Exception as e:
        print(e)
        print(
            "No r_ecoplatform_metadata table or date found, previous execution may be corrupted. New schema will created from zero.")
        cur.close()
        con.close()
        return None

    cur.close()
    con.close()
    return backup_schema_name


def create_schema(host, port, database, schema, username, password):
    """
    Create a new schema with the dataset name

    :param host: DB host
    :param port: DB port
    :param database: DB name
    :param schema: DB postgres schema (i.e. dataset name)
    :param username: DB username
    :param password: DB password
    :return: schema name as a String
    """
    con = pg.connect(dbname=database, host=host, port=port, user=username, password=password, sslmode=None)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    try:
        # Drop previous version schema. If it exists at this point, it is corrupted or wrong
        cur.execute(sql.SQL("DROP SCHEMA IF EXISTS {} CASCADE;").format(sql.Identifier(schema)))
        cur.execute(
            sql.SQL("CREATE SCHEMA {} AUTHORIZATION {};").format(sql.Identifier(schema), sql.Identifier(username)))
        print("New schema '{}' created with success.".format(schema))
    except Exception as e:
        print(e)
        cur.close()
        con.close()
        exit(1)

    cur.close()
    con.close()


def create_table_metadata(host, port, database, schema, username, password):
    """
    Create the metadata table (r_ecoplatform_metadata) in the specified schema, and insert the execution time as
    "load_time" (when the ingestion was performed)

    :param host: DB host
    :param port: DB port
    :param database: DB name
    :param schema: DB postgres schema (i.e. dataset name)
    :param username: DB username
    :param password: DB password
    """
    con = pg.connect(dbname=database, host=host, port=port, user=username, password=password)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()
    try:
        cur.execute(
            sql.SQL("CREATE TABLE {}.r_ecoplatform_metadata ( load_date date NULL );").format(sql.Identifier(schema)))
        cur.execute(
            sql.SQL("INSERT INTO {}.r_ecoplatform_metadata (load_date) VALUES (now());").format(sql.Identifier(schema)))
    except Exception as e:
        print(e)
        cur.close()
        con.close()
        exit(1)

    cur.close()
    con.close()


def insert_execution_metadata(host, port, username, password, database, executions_schema, dataset, table, inserts,
                              deletions, updates, airflow_dagrun_id):
    """
    Insert the execution metadata: Number of inserts, deletions and updates between both current and previous versions
    of the datasets.

    :param host: DB host
    :param port: DB port
    :param username: DB username
    :param password:  DB password
    :param database: DB name
    :param executions_schema: DB executions metadata schema
    :param dataset: dataset name
    :param table: table name
    :param inserts: Number of inserts in the new version of the dataset
    :param deletions: Number of deletions in the new version of the dataset
    :param updates: Number of updates in the new version of the dataset
    :param updates: Airflow execution DAG_RUN_ID
    """
    con = pg.connect(dbname=database, host=host, port=port, user=username, password=password)
    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()
    try:
        cur.execute(sql.SQL(
            """ INSERT INTO {}.data_ingestion_exec (dataset, "table", nof_insertions, nof_deletions, nof_updates, airflow_dagrun_id) 
                VALUES('{}', '{}', {}, {}, {}, '{}'); """.format(executions_schema, dataset, table, inserts, deletions,
                                                                 updates, airflow_dagrun_id)))
    except Exception as e:
        print(e)
        cur.close()
        con.close()
        exit(1)

    cur.close()
    con.close()


def get_obsolete_schema(host, port, database, schema, username, password):
    """
    Retrieve the oldest schema (i.e. oldest version of the data) for a specific dataset.

    :param host: DB host
    :param port: DB port
    :param database: DB name
    :param schema: DB postgres schema (i.e. dataset name)
    :param username: DB username
    :param password: DB password
    """
    con = pg.connect(dbname=database, host=host, port=port, user=username, password=password)
    cur = con.cursor()
    try:
        cur.execute(sql.SQL(
            "SELECT schema_name "
            "FROM information_schema.schemata "
            "WHERE schema_name LIKE '{}_%' "
            "ORDER BY schema_name LIMIT 1").format(sql.Identifier(schema)))
    except Exception as e:
        print(e)
        cur.close()
        con.close()
        exit(1)

    obsolete_schema = cur.fetchone()[0]

    cur.close()
    con.close()

    return obsolete_schema
