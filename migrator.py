import argparse
import json
import os
from urllib.parse import urlparse

from pyspark.sql import SparkSession

from database import (
    create_schema,
    create_table_metadata,
    get_postgres_url,
    insert_execution_metadata,
    load_table_h2,
    load_table_pg,
    rename_schema,
    save_table_pg,
)
from diff_spark import perform_database_diff

CORVEG = "corveg"
AUSPLOTS = "ausplots"

DIR_BASE = os.path.dirname(os.path.realpath(__file__))


def init_migration(dataset):
    # Load dataset specific configuration parameters
    if dataset.lower() == CORVEG:
        with open(DIR_BASE + "/datasets/corveg.json", "r") as config_file:
            config = json.load(config_file)
    elif dataset.lower() == AUSPLOTS:
        with open(DIR_BASE + "/datasets/ausplots.json", "r") as config_file:
            config = json.load(config_file)
    elif dataset.lower() == "test":
        print("Testing fast execution.... should finish fine.")
        exit(0)
    else:
        print("ERROR: No valid dataset specified.")
        return

    dbc = urlparse(os.getenv("POSTGRES_URL"))
    pg_host = dbc.hostname
    pg_port = dbc.port
    pg_database = dbc.path.lstrip("/")
    pg_username = dbc.username
    pg_password = dbc.password
    pg_schema = config["postgres"]["dataset_schema"]

    # Spark context
    spark = (
        SparkSession.builder.master(config["spark"]["master"])
        .appName(config["spark"]["app_name"])
        .getOrCreate()
    )

    # Rename previous schema for dataset to keep it as backup, and create a new schema for the updated data
    rename_schema(pg_host, pg_port, pg_database, pg_schema, pg_username, pg_password)
    create_schema(pg_host, pg_port, pg_database, pg_schema, pg_username, pg_password)
    # Migrate data from H2 to Postgres
    for table in config["tables"]:
        df = load_table_h2(
            spark,
            config["h2"]["url"],
            "{}.{}".format(config["h2"]["schema"], table),
            config["table_schemas"][table],
        )
        save_table_pg(
            df,
            get_postgres_url(pg_host, pg_port, pg_database),
            pg_schema,
            table,
            pg_username,
            pg_password,
        )

    # Calculate the differences between 2 data versions (new data ingested and previous one) for the specified tables
    # indicated in the config file. Store this information in the "executions" schema.
    # if old_schema is not None:
    #     for table in config["diff_tables"]:
    #         calculate_execution_metadata(spark, pg_host, pg_port, pg_database, pg_username, pg_password, pg_schema,
    #                                      old_schema, pg_executions_schema, dataset.lower(), table, dagrun_id)

    # Create the ecoplatform_metadata table and insert the ingestion date (now())
    create_table_metadata(pg_host, pg_port, pg_database, pg_schema, pg_username, pg_password)

    # Set a return dictionary for use in conjunction with Airflow. Subsequent Airflow tasks can read this dictionary
    # through the XCOM pull feature.
    # dump_schema = get_obsolete_schema(pg_host, pg_port, pg_database, pg_schema, pg_username, pg_password)
    # xcom_return = {"source_task": "h2-to-postgres-migrator", "current_db": pg_schema, "previous_db": old_schema,
    #                "dump_db": dump_schema}
    # with open("/airflow/xcom/return.json", "w") as file:
    #     json.dump(xcom_return, file)


def calculate_execution_metadata(
    spark,
    host,
    port,
    database,
    username,
    password,
    schema,
    old_schema,
    executions_schema,
    dataset,
    table,
    dagrun_id,
):
    """
    Calculate the differences between 2 data versions (new data ingested and previous one) for the specified tables
    indicated in the config file. Store this information in the "executions" schema.

    :param spark: SparkSession used to load data from Postgres to Dataframe
    :param host: DB host
    :param port: DB port
    :param database: DB name
    :param username: DB username
    :param password: DB password
    :param schema: dataset current schema (i.e. dataset name)
    :param old_schema: dataset previous schema (i.e. dataset name + ingestion date)
    :param executions_schema: executions schema for storing metadata
    :param dataset: dataset name
    :param table: table name

    """

    # Load current data version into Dataframe
    df_new = load_table_pg(
        spark,
        get_postgres_url(host, port, database, username, password),
        database,
        schema,
        table,
        username,
        password,
    )
    # Load previous data version into Dataframe
    df_old = load_table_pg(
        spark,
        get_postgres_url(host, port, database, username, password),
        database,
        old_schema,
        table,
        username,
        password,
    )

    # Calculate the differences in data between both versions of the table
    inserts_df, deletes_df, updates_df = perform_database_diff(spark, df_new, df_old)
    # Insert differences into database (executions schema and table) for later querying
    insert_execution_metadata(
        host,
        port,
        username,
        password,
        database,
        executions_schema,
        dataset,
        table,
        int(inserts_df.count()),
        int(deletes_df.count()),
        int(updates_df.count()),
        dagrun_id,
    )


def cli():
    parser = argparse.ArgumentParser(description="Data migration from H2 to Postgres")
    parser.add_argument("--dataset", action="store", required=True)
    return parser.parse_args()


def main(args):
    init_migration(args.dataset)


if __name__ == "__main__":
    args = cli()
    main(args)
