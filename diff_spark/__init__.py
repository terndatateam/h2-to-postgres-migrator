def perform_database_diff(spark, df_new, df_old):
    """
    Perform the difference between two versions of the same table (aka dataframes).

    :param spark:
    :param df_new:
    :param df_old:
    :return:
    """

    # df_new.repartition(Config.CPU_COUNT)
    # df_old.repartition(Config.CPU_COUNT)

    columns_list = df_new.columns
    insert_df = df_new.alias("new").join(df_old.alias("old"), on=[columns_list[0]], how="leftanti")
    delete_df = df_old.alias("old").join(df_new.alias("new"), on=[columns_list[0]], how="leftanti")

    df_new.createOrReplaceTempView("new")
    df_old.createOrReplaceTempView("old")
    update_df = spark.sql(identify_updates_query(columns_list))
    # print("New rows detected: ")
    # insert_df.show()
    # print("Deleted rows detected: ")
    # delete_df.show()
    # print("Updates rows detected: ")
    # update_df.show()
    return insert_df, delete_df, update_df


def identify_updates_query(columns):
    """
    Build a SQL query to find any differences in rows for two versions of a table.

    :param columns: Full list of columns of the table
    :return: Query as a String
    """
    query = "SELECT " + ", ".join(map(lambda col: 'new.' + col + ' AS n_' + col, columns)) + ", " + ", ".join(
        map(lambda col: 'old.' + col + ' AS o_' + col,
            columns)) + " from new join old on new.{0}=old.{0} where ".format(columns[0])
    for i in range(0, len(columns)):
        query += "new.{0}<>old.{0}".format(columns[i])
        if i < len(columns) - 1:
            query += " OR "
    print(query)
    return query
